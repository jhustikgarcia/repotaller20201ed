var productosObtenidos;

function getProductos() {
   var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";

   var request = new XMLHttpRequest();

   request.onreadystatechange = function(){

     if(this.readyState == 4 && this.status == 200){
       //console.table(JSON.parse(request.responseText).value);
       productosObtenidos = request.responseText;
       procesarProductos();
     }
   }

   request.open("GET", url, true);
   request.send();
}

function procesarProductos(){
  var jsonProductos = JSON.parse(productosObtenidos).value;
  var divTablaProductos = document.getElementById("divTablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var thead = document.createElement("thead");
  var filaHead = document.createElement("tr");
  var nameColumns = ["Nonmre de producto", "Precio", "Stock"];

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var i = 0; i < nameColumns.length; i ++){
    var headName = document.createElement("th");
    headName.innerText = nameColumns[i];
    filaHead.appendChild(headName);
  }

  for(var i=0; i < jsonProductos.length; i++){
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = jsonProductos[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = jsonProductos[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = jsonProductos[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(filaHead);
  tabla.appendChild(tbody);
  divTablaProductos.appendChild(tabla);
}
