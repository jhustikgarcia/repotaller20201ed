var clientesObtenidos;

function getClientes() {
   var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";

   var request = new XMLHttpRequest();

   request.onreadystatechange = function(){

     if(this.readyState == 4 && this.status == 200){
       //console.table(JSON.parse(request.responseText).value);
       clientesObtenidos = request.responseText;
       procesarClientes();
     }
   }

   request.open("GET", url, true);
   request.send();
}

function procesarClientes(){
  var jsonClientes = JSON.parse(clientesObtenidos).value;
  var divTablaClientes = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var thead = document.createElement("thead");
  var filaHead = document.createElement("tr");
  var nameColumns = ["Nonmre de contacto", "Ciudad", "Bandera"];
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  thead.classList.add("thead-light");

  for(var i = 0; i < nameColumns.length; i ++){
    var headName = document.createElement("th");
    headName.innerText = nameColumns[i];
    filaHead.appendChild(headName);
  }

  for(var i=0; i < jsonClientes.length; i++){
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = jsonClientes[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = jsonClientes[i].City;

    var columnaStock = document.createElement("td");
    var imgBandera = document.createElement("img");

    if(jsonClientes[i].Country == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    }else{
      imgBandera.src = rutaBandera + jsonClientes[i].Country + ".png";
    }

    imgBandera.style.width = "50px";
    imgBandera.style.height = "30px";
    columnaStock.innerText = jsonClientes[i].Country;
    columnaStock.appendChild(imgBandera);
    //columnaStock.innerText = jsonClientes[i].Country;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(filaHead);
  tabla.appendChild(tbody);
  divTablaClientes.appendChild(tabla);
}
